import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';


@Injectable()
export class DataRepositoryService {

	private apiUrl: string = environment.apiUrl;

	constructor(private http: HttpClient) {

	}

	private setHeaders() {
		var headers = new HttpHeaders();
		headers = headers.set('Content-Type', 'application/json');
		headers = headers.set('Accept', 'application/json');

		return headers;
	}

	getRequest(apiRoute: string) {
		return this.http.get(this.apiUrl + apiRoute, {
			headers: this.setHeaders(),
		});
	}

	postRequest(data: any, apiRoute: string) {
		return this.http.post(this.apiUrl + apiRoute, data, {
			headers: this.setHeaders(),
		});
	}

	putRequest(data: any, apiRoute: string) {
		const body = data;

		return this.http.put(this.apiUrl + apiRoute, body, {
			headers: this.setHeaders(),
		});
	}

	deleteRequest(apiRoute: string) {
		return this.http.delete(this.apiUrl + apiRoute, {
			headers: this.setHeaders(),
		});
  }



}
