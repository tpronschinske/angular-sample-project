import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ClarityModule } from '@clr/angular';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/app/header/header.component';
import { HomeComponent } from './views/home/home.component';
import { CustomerSearchPipe } from './pipes/customer-fliter.pipe';
import { FilterComponent } from './components/filter/filter.component';
import { DataRepositoryService } from './services/data-repository.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FilterComponent,
    CustomerSearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ClarityModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    DataRepositoryService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
