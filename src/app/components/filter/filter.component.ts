import { Component, OnInit, Input } from '@angular/core';
import { Customer } from 'src/app/models/customer.model';

@Component({
	selector: 'app-filter',
  templateUrl: 'filter.component.html',
  styleUrls: ['./filter.component.css']
})

export class FilterComponent implements OnInit {

  public search: string;

  @Input() label: string;
  @Input() listItems: Array<Customer>;

  ngOnInit() { }

}
