import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customerFilter'
})
export class CustomerSearchPipe implements PipeTransform {
    transform(items: any[], searchTerm: string): any {
      if (!items)
        return [];

      if (!searchTerm)
        return items;

      if(searchTerm)
          return items.filter((f: any) => {
          return f.CompanyName.toUpperCase().includes(searchTerm.toUpperCase());
      });
  }
}
