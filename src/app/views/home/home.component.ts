import { DataRepositoryService } from '../../services/data-repository.service';
import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/models/customer.model';
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-home',
	templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit {

  public errorMsg: string = 'Failed to retrieve customer list.';
  public customerList: Array<Customer>;

  constructor(public dataService: DataRepositoryService, public toastr: ToastrService){

  }

	ngOnInit() {
    this.getCustomerList();
  }

  getCustomerList(){
    this.dataService.getRequest('customer/list').subscribe((result: any) => {
      this.customerList = result.data;
    }, (error: any) => {
        this.toastr.error(this.errorMsg)
    })
  }

}
